package TC20.TC20;

import dto.Geometria;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
    

    
{
    Geometria cuadrado = new Geometria(1);
    Geometria circulo = new Geometria(2);
    Geometria triangulo = new Geometria(3);
    Geometria rectangulo = new Geometria(4);
    Geometria pentagono = new Geometria(5);
    Geometria rombo = new Geometria(6);
    Geometria romboide = new Geometria(7);
    Geometria trapecio = new Geometria(8);
    Geometria defau = new Geometria(16);
    Geometria defa = new Geometria();
    
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    
    public void testAreaCuadrado() {
        int resultado = Geometria.areacuadrado(2);
        int esperado = 4;
        assertEquals(esperado, resultado);
    }
    
    public void testAreaCirculo() {
        double resultado = Geometria.areaCirculo(6);
        double esperado = 113.0976;
        assertEquals(esperado, resultado);
    }
    
    public void testAreaTriangulo() {
        int resultado = Geometria.areatriangulo(2, 4);
        int esperado = 4;
        assertEquals(esperado, resultado);
    }
    
    public void testAreaRectangulo() {
        int resultado = Geometria.arearectangulo(2, 4);
        int esperado = 8;
        assertEquals(esperado, resultado);
    }
    
    public void testAreaPentagono() {
        int resultado = Geometria.areapentagono(2, 4);
        int esperado = 4;
        assertEquals(esperado, resultado);
    }
    
    public void testAreaRombo() {
        int resultado = Geometria.arearombo(2, 4);
        int esperado = 4;
        assertEquals(esperado, resultado);
    }
    
    public void testAreaRomboide() {
        int resultado = Geometria.arearomboide(2, 4);
        int esperado = 8;
        assertEquals(esperado, resultado);
    }
    
    public void testAreaTrapecio() {
        int resultado = Geometria.areatrapecio(2, 4, 6);
        int esperado = 18;
        assertEquals(esperado, resultado);
    }
    
    public void testGetId() {
        assertEquals(1, cuadrado.getId());
    }
    
    public void testGetNom() {
        assertEquals("cuadrado", cuadrado.getNom());
    }
    
    public void testGetArea() {
        assertEquals(0, cuadrado.getArea(), 1);
    }
    
    public void testSetNom() {
        cuadrado.setNom("triangulo");
        assertEquals("triangulo", cuadrado.getNom());
    }
    
    public void testSetId() {
        cuadrado.setId(2);
        assertEquals(2, cuadrado.getId());
    }
    
    public void testSetArea() {
    	cuadrado.setArea(2);
        assertEquals(1, cuadrado.getId());
    }
    
    public void testToString() {
        assertEquals("Geometria [id=1, nom=cuadrado, area=0.0]", cuadrado.toString());
    }
}
